package com.example.sqliteapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.sqliteapp.bean.Note;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MyDatabaseHelper extends SQLiteOpenHelper {

    public static final String TAG = "TAG";

    // Database version
    private static final int DATABASE_VERSION = 1;

    // Database name
    private static final String DATABASE_NAME = "Note_Manager";

    // Table name: Note
    private static final String TABLE_NOTE = "Note";

    // Columns name
    private static final String COLUMN_NOTE_ID = "Note_Id";
    private static final String COLUMN_NOTE_TITLE = "Note_Title";
    private static final String COLUMN_NOTE_CONTENT = "Note_Content";
    private static final String COLUMN_NOTE_DATE = "Note_Date";

    public MyDatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.i("MyDataHelper", "MyDatabaseHelper called ");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i("TAGonCreate", "MyDatabaseHelper.onCreate ... ");
        // Script to create the table whose table is TABLE_NOTE
        // 3 columns: COLUMN_NOTE_ID; COLUMN_NOTE_TITLE; COLUMN_NOTE_CONTENT;
        String script = "CREATE TABLE " + TABLE_NOTE + "("
                + COLUMN_NOTE_ID + " INTEGER PRIMARY KEY,"
                + COLUMN_NOTE_TITLE + " TEXT,"
                + COLUMN_NOTE_CONTENT + " TEXT,"
                + COLUMN_NOTE_DATE + " TEXT" + ")";
        // Execute script
        db.execSQL(script);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop table
        Log.i("TAG DROP", "Dropping the table");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTE);

        // Updating the Table
        Log.i("TAG UPDATE", "Adding a new column to the table");
        db.execSQL("ALTER TABLE foo ADD COLUMN Note_Date STRING DEFAULT 0");


        Log.i(TAG + " onUpgrade", "MyDatabaseHelper.onUpgrade...");
        //db.execSQL("ALTER TABLE foo ADD COLUMN " + COLUMN_NOTE_DATE + " STRING DEFAULT 0");

        // Recreate
        onCreate(db);
    }

    // If Note table has no Data
    // default, insert 2 records.
    public void createDefaultNotesIfNeed(){
        int count = this.getNotesCount();
        if(count == 0){
            Note note1 = new Note("Firstly see Android ListView", "See Android ListView Example","22/06/2021");
            Note note2 = new Note("Learning android sqlite", "See Android sqlite Example","16/06/2021");
            Note note3 = new Note("3rd note", "this is a test","28/06/2021");
            this.addNote(note1);
            this.addNote(note2);
            this.addNote(note3);
        }
    }

    // method to add notes to the database
    public void addNote(Note note){
        Log.i(TAG, "MyDatabaseHelper.addNote ... " + note.getNoteTitle());

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NOTE_TITLE,note.getNoteTitle());
        values.put(COLUMN_NOTE_CONTENT, note.getNoteContent());
        values.put(COLUMN_NOTE_DATE, note.getNoteDate());

        // Inserting row to the database
        db.insert(TABLE_NOTE, null, values);

        // Closing Database connexion
        db.close();

    }

    public Note getNote(int id){
        Log.i(TAG, "MyDatabaseHelper.getNote ... " + id);

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NOTE, new String[]{COLUMN_NOTE_ID, COLUMN_NOTE_TITLE, COLUMN_NOTE_CONTENT, COLUMN_NOTE_DATE}, COLUMN_NOTE_ID+"=?",new String[]{String.valueOf(id)}, null, null, null, null);
        if(cursor != null)
            cursor.moveToFirst();
        // Creating a note from the cursor
        Note note = new Note(Integer.parseInt(cursor.getString(0)),cursor.getString(1), cursor.getString(2), cursor.getString(3));
        // return the note
        cursor.close();
        return note;
    }

    public List<Note> getDateNotes(String date){
        Log.i(TAG + " getDateNotes", "MyDatabaseHelper.getDateNotes ... " );

        // List<Note> noteList = new ArrayList<Note>();

        //Select all querries at the given date
        String selectDateQuerry = "SELECT * FROM " +TABLE_NOTE +" WHERE "+COLUMN_NOTE_DATE + " = ?";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectDateQuerry,new String[] {date});
        if(cursor != null){
            List<Note> noteList = new ArrayList<Note>();
            // Looping through all rows and adding to the noteList
            if (cursor.moveToFirst()){
                do{
                    Note note = new Note();
                    note.setNoteId(Integer.parseInt(cursor.getString(0)));
                    note.setNoteTitle(cursor.getString(1));
                    note.setNoteContent(cursor.getString(2));
                    note.setNoteDate(cursor.getString(3));
                    // Adding note to list
                    noteList.add(note);
                } while (cursor.moveToNext());
            }
            return(noteList);
        }
        else
            return null;
    }

    public List<Note> getAllNotes(){
        Log.i(TAG, "MyDatabaseHelper.getAllNotes ... " );

        List<Note> noteList = new ArrayList<Note>();

        //Select all querries
        String selectQuerry = "SELECT * FROM "+TABLE_NOTE;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuerry,null);

        // Looping through all rows and adding to the noteList
        if (cursor.moveToFirst()){
            do{
                Note note = new Note();
                note.setNoteId(Integer.parseInt(cursor.getString(0)));
                note.setNoteTitle(cursor.getString(1));
                note.setNoteContent(cursor.getString(2));
                note.setNoteDate(cursor.getString(3));
                // Adding note to list
                noteList.add(note);
            } while (cursor.moveToNext());
        }
        return(noteList);
    }

    public int getNotesCount() {
        Log.i(TAG, "MyDatabaseHelper.getNotesCount ... " );

        String countQuery = "SELECT * FROM " + TABLE_NOTE;
        SQLiteDatabase db = this.getReadableDatabase();
        Log.i("getNotesCountFunction", "getReadableDatabase called");
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        return count;
    }

    public int updateNote(Note note){
        Log.i(TAG, "MyDatabaseHelper.updateNote ... "  + note.getNoteTitle());

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NOTE_TITLE, note.getNoteTitle());
        values.put(COLUMN_NOTE_CONTENT, note.getNoteContent());
        values.put(COLUMN_NOTE_DATE, note.getNoteDate());

        String where = COLUMN_NOTE_ID+"=?";
        String[] whereArgs = new String[] {String.valueOf(note.getNoteId())};
        // updating row
        return db.update(TABLE_NOTE, values, where , whereArgs);
    }

    public void deleteNote(Note note){
        Log.i(TAG, "MyDatabaseHelper.updateNote ... " + note.getNoteTitle() );

        SQLiteDatabase db = this.getWritableDatabase();
        String where = COLUMN_NOTE_ID+"=?";
        String[] whereArgs = new String[] {String.valueOf(note.getNoteId())};
        db.delete(TABLE_NOTE, where, whereArgs);
        db.close();
    }

    public void deleteAllNotes(){
        Log.i(TAG, "MyDatabaseHelper.getDeleteAllNotes ... " );

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NOTE,null, null);
    }
}
