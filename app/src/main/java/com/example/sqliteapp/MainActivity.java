package com.example.sqliteapp;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sqliteapp.bean.Note;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private CalendarView calendarView;
    private Calendar calendarInstance;
    public String currentDate;
    public static String selectedDate;
    public static SimpleDateFormat sdf;

    private static final int MENU_ITEM_VIEW = 111;
    private static final int MENU_ITEM_EDIT = 222;
    private static final int MENU_ITEM_DELETE = 444;

    private static final int MY_REQUEST_CODE = 1000;

    public final List<Note> noteList = new ArrayList<Note>();
    public final List<Note> noteDateList = new ArrayList<Note>();
    private ArrayAdapter<Note> listViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Connect the button
        Button buttonOne = findViewById(R.id.buttonAddNote);
        buttonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AddEditNoteActivity.class);
                someActivityResultLauncher.launch(intent);
            }
        });

        // Get ListView object from xml
        this.listView = (ListView) findViewById(R.id.listView);
        this.calendarView = (CalendarView) findViewById(R.id.calendarView);
        this.calendarView.setFirstDayOfWeek(2); // set Monday as the first day of the week

        // Get a date, in format "dd/MM/yyyy"
        this.calendarInstance = Calendar.getInstance();
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        currentDate = sdf.format(new Date(calendarView.getDate()));
        Log.i("selectedDate", "value = " + currentDate);

        // Call de database
        MyDatabaseHelper db = new MyDatabaseHelper(this);
        db.createDefaultNotesIfNeed();
        List<Note> list = db.getAllNotes();
        this.noteList.addAll(list);
        listViewAdapter = new ArrayAdapter<Note>(getApplicationContext(), android.R.layout.simple_list_item_1, android.R.id.text1, noteList);

        // Set a onSelectDateChange
        this.calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener(){
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                int month_padded = month + 1;
                selectedDate = dateFormater(year, month_padded, dayOfMonth);
                Log.d("selectedDate", selectedDate);
                List<Note> dateList = db.getDateNotes(dateFormater(year, month_padded, dayOfMonth));
                if(noteList.isEmpty() != true){
                    noteDateList.clear();
                    noteDateList.addAll(dateList);
                    listViewAdapter = new ArrayAdapter<Note>(getApplicationContext(), android.R.layout.simple_list_item_1, android.R.id.text1, noteDateList);
                    listViewAdapter.notifyDataSetChanged();
                    refreshAdapter();
                }
            }
        });
    }

    public void refreshAdapter(){
        this.listView.setAdapter(this.listViewAdapter);
        registerForContextMenu(this.listView);
    }

    private String dateFormater (int year, int month, int dayOfMonth){
        String paddedMonth = String.valueOf(month);
        String paddedDayOfMonth =String.valueOf(dayOfMonth);

        if(dayOfMonth<10)
            paddedDayOfMonth = "0" + dayOfMonth;
        if(month<10)
            paddedMonth = "0" + month;

        return paddedDayOfMonth + "/" + paddedMonth + "/" + year;
    };

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);
        menu.setHeaderTitle("Select the Action");

        menu.add(0, MENU_ITEM_VIEW, 0, "View note");
        menu.add(0, MENU_ITEM_EDIT, 2, "Edit Note");
        menu.add(0, MENU_ITEM_DELETE, 4, "Delete Note");
    }


    private ActivityResultLauncher<Intent> someActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    // There are no request codes
                    Intent data = result.getData();
                    boolean needRefresh = data.getBooleanExtra("needRefresh", true);
                    // Refresh ListView
                    if (needRefresh) {
                        MainActivity.this.noteList.clear();
                        MyDatabaseHelper db = new MyDatabaseHelper(MainActivity.this);
                        List<Note> list = db.getAllNotes();
                        MainActivity.this.noteList.addAll(list);
                        // Notify the data change (To refresh the ListView).
                        MainActivity.this.listViewAdapter.notifyDataSetChanged();
                    }
                }
            });

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo
                info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        final Note selectedNote = (Note) this.listView.getItemAtPosition(info.position);

        if (item.getItemId() == MENU_ITEM_VIEW) {
            Toast.makeText(getApplicationContext(), selectedNote.getNoteContent(), Toast.LENGTH_LONG).show();
        }
        else if (item.getItemId() == MENU_ITEM_EDIT) {
            Log.d("test1","MENU_ITEM_EDIT selected");
            Intent intent = new Intent(this, AddEditNoteActivity.class);
            intent.putExtra("note", selectedNote);
            someActivityResultLauncher.launch(intent);
        } else if (item.getItemId() == MENU_ITEM_DELETE) {
            // Ask before deleting.
            new AlertDialog.Builder(this)
                    .setMessage(selectedNote.getNoteTitle() + ". Are you sure you want to delete?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            deleteNote(selectedNote);
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        } else {
            return false;
        }
        return true;
    }

    // Delete a record
    private void deleteNote(Note note) {
        MyDatabaseHelper db = new MyDatabaseHelper(this);
        db.deleteNote(note);
        this.noteList.remove(note);
        // Refresh ListView
        this.listViewAdapter.notifyDataSetChanged();
    }
}
