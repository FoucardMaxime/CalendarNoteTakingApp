Dear Reader,

This project uses JAVA on Android Studio.

This project is a note taking app linked to a calendar. Each day, the user can create several notes where he can specify a title and a content.
The data is stored in a database accessed with SQLite.
